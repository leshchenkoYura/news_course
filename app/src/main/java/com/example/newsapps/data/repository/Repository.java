package com.example.newsapps.data.repository;

import com.example.newsapps.data.model.api.country.ResponseCountry;

import java.util.List;

import io.reactivex.Single;

public interface Repository {
    interface Remote {
        Single<ResponseCountry> getResponseCountry();
        Single<ResponseCountry> read();
    }

    interface Locale {

    }
}
