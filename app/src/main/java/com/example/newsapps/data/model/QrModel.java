package com.example.newsapps.data.model;

import com.google.gson.annotations.SerializedName;

public class QrModel {
    @SerializedName("message")
    private String massage;

    public QrModel(String massage) {
        this.massage = massage;
    }

    @Override
    public String toString() {
        return "QrModel{" +
                "massage='" + massage + '\'' +
                '}';
    }
}
