package com.example.newsapps.data.repository;

import com.example.newsapps.data.model.api.country.ResponseCountry;
import com.example.newsapps.data.remout_storege.RestAdapter;
import com.example.newsapps.data.remout_storege.ServiceApi;
import com.google.gson.Gson;

import java.util.List;

import io.reactivex.Single;

public class RepositoryRemote implements Repository.Remote {
    private static Repository.Remote instance;
    private ServiceApi.Api api;
    private Gson gson;

    public RepositoryRemote() {
        api = RestAdapter.getInstanse().getApi();
        gson = RestAdapter.getInstanse().getGson();
    }

    public static synchronized Repository.Remote getInstance(){
        if (instance == null){
            instance = new RepositoryRemote();
        }
        return instance;
    }

    @Override
    public Single<ResponseCountry> getResponseCountry() {
        ResponseCountry country = new ResponseCountry();
        String str = gson.toJson(country);
        ResponseCountry country1 = gson.fromJson(str,ResponseCountry.class);
        return api.requestGroup("91d2f658828441ff99fd3c7a15d56f18");
    }

    @Override
    public Single<ResponseCountry> read() {
        return api.requestGroup("91d2f658828441ff99fd3c7a15d56f18");
    }
}
