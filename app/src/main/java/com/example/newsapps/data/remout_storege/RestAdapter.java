package com.example.newsapps.data.remout_storege;


import com.example.newsapps.BuildConfig;
import com.example.newsapps.Constanta;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestAdapter implements ServiceApi {
    private static ServiceApi instance;
    private Gson gson;
    private ServiceApi.Api api;

    private RestAdapter(){
        gson = initGson();
        api = createApi(ServiceApi.Api.class,okHttpGenerate(), GsonConverterFactory.create(gson));
    }

    public static synchronized ServiceApi getInstanse(){
        if (instance == null){
            instance = new RestAdapter();
        }
        return instance;
    }


    private Gson initGson(){
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return gsonBuilder.setLenient()
                .create();
    }

    private OkHttpClient okHttpGenerate(){
        final OkHttpClient.Builder builder = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG){
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.level(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(logging);
        }
        return builder.connectTimeout(60 * 1000, TimeUnit.MILLISECONDS)
                .readTimeout(60 * 1000, TimeUnit.MILLISECONDS)
                .build();
    }

    private <T> T createApi(Class<T> serviceApi, OkHttpClient okHttpClient, Converter.Factory factoryConverter) {
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.client(okHttpClient)
                .baseUrl(Constanta.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(factoryConverter);
        return builder.build().create(serviceApi);
    }

    @Override
    public Api getApi() {
        return api;
    }

    @Override
    public Gson getGson() {
        return gson;
    }
}
