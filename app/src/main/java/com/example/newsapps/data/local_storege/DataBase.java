package com.example.newsapps.data.local_storege;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.newsapps.data.model.dao.EntityNews;

@Database(entities = {EntityNews.class},version = 1,exportSchema = false)
public abstract class DataBase extends RoomDatabase {
    public abstract LocalDAO getPersonDao();
}
