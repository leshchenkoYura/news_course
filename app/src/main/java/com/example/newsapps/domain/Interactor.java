package com.example.newsapps.domain;

public interface Interactor<Listener> {
    void onStart(Listener listener);

    void onStop();
}
