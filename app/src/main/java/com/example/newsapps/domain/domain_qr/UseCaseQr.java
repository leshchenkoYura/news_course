package com.example.newsapps.domain.domain_qr;

import com.example.newsapps.data.model.QrModel;
import com.example.newsapps.data.remout_storege.RestAdapter;
import com.example.newsapps.domain.BaseInteractor;
import com.google.zxing.BarcodeFormat;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public class UseCaseQr extends BaseInteractor implements InteractorQr.UseCaseQr {
    private InteractorQr.Listener listener;
    private Disposable disposable;


    @Override
    public void generateQr(String msg) {
         disposable = Single.defer(() -> Single.just(msg))
                .compose(applySingleSchedulers())
                .flatMap(v -> Single.just(new BarcodeEncoder()
                        .encodeBitmap(RestAdapter.getInstanse()
                                .getGson().toJson(new QrModel(msg)),BarcodeFormat.QR_CODE, 200, 200)))
                .doOnError(e -> Timber.tag(UseCaseQr.class.getSimpleName()).e(e))
                 .subscribe(bitmap -> {
                     if (listener != null) listener.onSuccess(bitmap);
                 },e -> {
                     if (listener != null) listener.onError(e.getMessage());
                 });
    }

    @Override
    public void onStart(InteractorQr.Listener listener) {
        this.listener = listener;
    }

    @Override
    public void onStop() {
        if (disposable != null && !disposable.isDisposed()){
            disposable.dispose();
            disposable = null;
        }
        listener = null;
    }
}
