package com.example.newsapps.domain.domain_qr;


import android.graphics.Bitmap;

import com.example.newsapps.domain.Interactor;

public interface InteractorQr {
    interface Listener{
       void onSuccess(Bitmap bitmap);
       void onError(String err);
    }

    interface UseCaseQr extends Interactor<Listener> {
       void generateQr(String msg);
    }

}
