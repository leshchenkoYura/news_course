package com.example.newsapps.domain.domain_country;

import android.graphics.Bitmap;

import com.example.newsapps.data.model.api.country.ResponseCountry;
import com.example.newsapps.domain.Interactor;

import java.util.List;

import io.reactivex.Single;

public interface InteractorCountry {
    interface Listener{
        void onSuccess();
        void onError(String msg);
    }

    interface UseCaseCountry extends Interactor<Listener> {
        void getResponceCountry();
        Single<ResponseCountry> read();

    }
}
