package com.example.newsapps.domain.domain_country;

import com.example.newsapps.data.model.api.country.ResponseCountry;
import com.example.newsapps.data.repository.RepositoryRemote;
import com.example.newsapps.domain.BaseInteractor;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import timber.log.Timber;

public class UseCaseCountry extends BaseInteractor implements InteractorCountry.UseCaseCountry {
    private InteractorCountry.Listener listener;
    private Disposable disposable;

    @Override
    public void onStart(InteractorCountry.Listener listener) {
        this.listener = listener;
    }

    @Override
    public void onStop() {
        if (disposable != null && !disposable.isDisposed()){
            disposable.dispose();
            disposable = null;
        }
        listener = null;
    }

    @Override
    public void getResponceCountry() {
         RepositoryRemote.getInstance().getResponseCountry()
                .compose(applySingleSchedulers())
                .doOnError(e -> Timber.e("getResponceCountry doOnError %s",e.getMessage()))
                .subscribe(new DisposableSingleObserver<ResponseCountry>() {
                    @Override
                    public void onSuccess(ResponseCountry responseCountry) {
                        Timber.e("getResponceCountry onSuccess %s",responseCountry);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e("getResponceCountry onError %s",e.getMessage());
                    }
                });
    }

    @Override
    public Single<ResponseCountry> read() {
        return Single.defer(() -> RepositoryRemote.getInstance().read())
                .compose(applySingleSchedulers());
    }
}
