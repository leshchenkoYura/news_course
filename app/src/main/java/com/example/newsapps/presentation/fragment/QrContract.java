package com.example.newsapps.presentation.fragment;

import android.graphics.Bitmap;

import com.example.newsapps.presentation.base.BasePresenter;

public interface QrContract {
    interface View{
        void showQr(Bitmap bitmap);

        String getInfo();

        void toast(String msg);
    }

    interface Presenter extends BasePresenter<View>{
        void generateQr();
    }
}
