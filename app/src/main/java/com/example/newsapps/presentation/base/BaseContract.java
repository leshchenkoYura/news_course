package com.example.newsapps.presentation.base;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;


public interface BaseContract {
    void goToFragment(Fragment fragment, int container);
    void openDialog(DialogFragment fragment, int container);
    void closeFragmentDialog(DialogFragment fragment);
    void onBackPressed();
    <T>void transactionActivity(Class<?> activity,boolean cycleFinish,T object);
    void showBackStack(boolean flag);
    void setAppBarText(String string);
}
