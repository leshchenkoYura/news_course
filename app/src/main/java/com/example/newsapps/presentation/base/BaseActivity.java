package com.example.newsapps.presentation.base;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.example.newsapps.R;

public abstract class BaseActivity<Binding extends ViewDataBinding> extends AppCompatActivity {
    private Binding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, getResLayout());
        initView(savedInstanceState);
    }

    protected abstract void initView(@Nullable Bundle savedInstanceState);

    @LayoutRes
    protected abstract int getResLayout();

    protected abstract void startView();

    protected abstract BasePresenter getPresenter();

    protected Binding getBinding() {
        return binding;
    }

    @Override
    protected void onStart() {
        super.onStart();
        startView();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (getPresenter() != null) {
            getPresenter().onStopView();
        }
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    protected void goToFragmentWithBackStack(Fragment fragment, int container) {
        this.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fragment_enter, R.anim.fragment_exit,
                        R.anim.fragment_pop_enter, R.anim.fragment_pop_exit)
                .replace(container, fragment, fragment.getClass().getSimpleName())
                .addToBackStack(null)
                .commit();
    }

    protected void openFragmentDialogCreate(DialogFragment fragment, int container){
        this.getSupportFragmentManager()
                .beginTransaction()
                .add(container,fragment,fragment.getClass().getSimpleName())
                .commit();
    }
    protected void openFragmentDialogUpdate(DialogFragment fragment, int container){
        this.getSupportFragmentManager()
                .beginTransaction()
                .add(container,fragment,fragment.getClass().getSimpleName())
                .commit();
    }

    protected void toastLong(String message){
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
    }

    protected <T>void transactionActivity(Class<?> activity,boolean cycleFinish,T object){
        if (activity != null) {
            Intent intent = new Intent(this, activity);

            startActivity(intent);
            if(cycleFinish) {
                this.finish();
            }
        }
    }
}
