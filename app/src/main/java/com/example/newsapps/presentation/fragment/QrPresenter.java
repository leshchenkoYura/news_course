package com.example.newsapps.presentation.fragment;

import android.graphics.Bitmap;

import com.example.newsapps.domain.domain_qr.InteractorQr;
import com.example.newsapps.domain.domain_qr.UseCaseQr;

public class QrPresenter implements QrContract.Presenter,InteractorQr.Listener {
    private QrContract.View view;
    private InteractorQr.UseCaseQr qr;

    public QrPresenter() {
        qr = new UseCaseQr();
        qr.onStart(this);
    }

    @Override
    public void onStartView(QrContract.View view) {
        this.view = view;
    }

    @Override
    public void onStopView() {
        view = null;
        if (qr != null){
            qr.onStop();
            qr = null;
        }
    }

    @Override
    public void generateQr() {
        String info = view.getInfo();
        if (info == null || info.isEmpty()){
            view.toast("Введите значение");
            return;
        }
        qr.generateQr(view.getInfo());
    }

    @Override
    public void onSuccess(Bitmap bitmap) {
        if (view != null) view.showQr(bitmap);
    }

    @Override
    public void onError(String err) {
        if (view != null) view.toast(err);
    }
}
