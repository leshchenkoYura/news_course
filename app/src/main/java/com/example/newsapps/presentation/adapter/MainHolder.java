package com.example.newsapps.presentation.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.newsapps.databinding.MainItemBinding;
import com.example.newsapps.presentation.fragment.main.MainFragmentContract;


public class MainHolder extends RecyclerView.ViewHolder {
    private Context context;
    private MainFragmentContract.Presenter presenter;
    private MainItemBinding binding;

    public MainHolder(@NonNull View itemView, Context context, MainFragmentContract.Presenter presenter) {
        super(itemView);
        binding = DataBindingUtil.bind(itemView);
        this.context = context;
        this.presenter = presenter;
    }

    public void bind(String title, String url, int position){
        Glide
        .with(context)
                .load(url)
                .centerCrop()
                .into((ImageView) itemView);

    }
}
