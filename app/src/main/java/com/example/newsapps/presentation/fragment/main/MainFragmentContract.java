package com.example.newsapps.presentation.fragment.main;

import com.example.newsapps.presentation.base.BasePresenter;

import java.util.Map;

public interface MainFragmentContract {
    interface View{
        void initAdapter(String string);
        void messageError(String msg);

    }

    interface Presenter extends BasePresenter<View> {
        void init();

    }
}
