package com.example.newsapps.presentation.view;

import android.content.Context;
import android.graphics.Bitmap;

import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.newsapps.R;


public class QRCodeView extends ConstraintLayout {

    private ImageView ivQR;
    private ConstraintLayout viewGroupToAnimate;

    public QRCodeView(Context context) {
        super(context);
        init(context);
    }

    public QRCodeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public QRCodeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context){
        LayoutInflater mInflater = LayoutInflater.from(context);
        View myView = mInflater.inflate(R.layout.qr_code_view, null);
        ConstraintLayout.LayoutParams parentParams = new ConstraintLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        addView(myView, parentParams);
        viewGroupToAnimate = myView.findViewById(R.id.cl_shadow);
        ivQR = myView.findViewById(R.id.iv_qr);
        showQrAnimation(viewGroupToAnimate);

    }

    private void showQrAnimation(final View view) {
        Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.qr_animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override

            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        view.startAnimation(animation);
    }

    public void setImageBitmap(Bitmap bitmap) {
        ivQR.setImageBitmap(bitmap);
    }

    @Override
    public void setVisibility(int visibility){
        super.setVisibility(visibility);
        if (visibility == VISIBLE){
            showQrAnimation(viewGroupToAnimate);
        }
    }
}