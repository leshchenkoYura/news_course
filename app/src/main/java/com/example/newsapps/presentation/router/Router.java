package com.example.newsapps.presentation.router;

import com.example.newsapps.R;
import com.example.newsapps.data.model.Data;
import com.example.newsapps.presentation.activity.activity_main.MainActivity;
import com.example.newsapps.presentation.base.BaseContract;
import com.example.newsapps.presentation.fragment.QRFragment;
import com.example.newsapps.presentation.fragment.main.MainFragment;

public class Router implements IRouter {
    private static IRouter instance;
    private BaseContract view;

    private Router() {
    }

    public static synchronized IRouter getInstance() {
        if (instance == null) {
            instance = new Router();
        }
        return instance;
    }

    @Override
    public void startView(BaseContract view) {
        this.view = view;
    }

    @Override
    public void stopView() {
        view = null;
    }

    @Override
    public void step(String tag, Data data) {
        switch (tag) {
            case Constant.FRAGMENT_QR:
                view.goToFragment(QRFragment.newInstance(), R.id.content_activity);
                break;
            case Constant.MAIN_ACTIVITY:
                view.transactionActivity(MainActivity.class, true, null);
                break;
            case Constant.FRAGMENT_MAIN:
                view.goToFragment(MainFragment.newInstance(), R.id.content_activity);
        }

    }

    @Override
    public void onBackPressed() {
        view.onBackPressed();
    }

    @Override
    public void showBackStack(boolean flag) {
        view.showBackStack(flag);
    }

    @Override
    public void setAppBarText(String string) {
        view.setAppBarText(string);
    }

    public static final class Constant{
        public final static String FRAGMENT_QR = "qr";
        public final static String MAIN_ACTIVITY = "main_activity";
        public final static String FRAGMENT_MAIN = "main_fragment";
    }
}
