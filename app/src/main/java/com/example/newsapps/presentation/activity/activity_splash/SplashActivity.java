package com.example.newsapps.presentation.activity.activity_splash;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.os.Bundle;

import com.example.newsapps.R;
import com.example.newsapps.databinding.ActivitySplashBinding;
import com.example.newsapps.presentation.base.BaseActivity;
import com.example.newsapps.presentation.base.BasePresenter;
import com.example.newsapps.presentation.router.Router;

public class SplashActivity extends BaseActivity<ActivitySplashBinding>  implements SplashView.View{
    private SplashView.Presenter presenter;



    @Override
    protected void initView(@Nullable Bundle savedInstanceState) {
        presenter = new SplashPresenter();
        Router.getInstance().startView(this);
        getBinding().setEvent(presenter);
        presenter.transaction(Router.Constant.MAIN_ACTIVITY);


//        new RxPermissions(this).request(Manifest.permission.CAMERA,
//                Manifest.permission.READ_PHONE_STATE,
//                Manifest.permission.ACCESS_FINE_LOCATION,
//                Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                .flatMap(flag -> {
//                    if (!flag) {
//                        toastLong("без этих разрешенией нет возможности использовать данное ПО");
//                        return Observable.timer(3, TimeUnit.SECONDS)
//                                .flatMap(v -> new RxPermissions(this).request(Manifest.permission.CAMERA,
//                                        Manifest.permission.READ_PHONE_STATE,
//                                        Manifest.permission.ACCESS_FINE_LOCATION,
//                                        Manifest.permission.WRITE_EXTERNAL_STORAGE));
//                    } else {
//                        return Observable.just(flag);
//                    }
//                })
//                .lastOrError()
//                .subscribe(new DisposableSingleObserver<Boolean>() {
//                    @Override
//                    public void onSuccess(Boolean aBoolean) {
//                        Timber.e(" RxPermissions onSuccess %s",aBoolean);
//                        if (aBoolean){
//                            presenter.transaction(Router.Constant.FRAGMENT_QR);
//                        }
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        Timber.e(" RxPermissions onError %s",e.getMessage());
//                    }
//                });
    }

    @Override
    protected int getResLayout() {
        return R.layout.activity_splash;
    }

    @Override
    protected void startView() {

    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void goToFragment(Fragment fragment, int container) {
        super.goToFragmentWithBackStack(fragment, container);
    }

    @Override
    public void openDialog(DialogFragment fragment, int container) {

    }

    @Override
    public void closeFragmentDialog(DialogFragment fragment) {

    }

    @Override
    public void onBackPressed() {
        int size = this.getSupportFragmentManager().getFragments().size();
        if (size == 1){
            super.finish();
        }
    }

    @Override
    public <T> void transactionActivity(Class<?> activity, boolean cycleFinish, T object) {
        super.transactionActivity(activity, cycleFinish, object);
    }

    @Override
    public void showBackStack(boolean flag) {

    }

    @Override
    public void setAppBarText(String string) {

    }

    //    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//    }
}