package com.example.newsapps.presentation.fragment.main;


import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.newsapps.R;
import com.example.newsapps.databinding.FragmentMainBinding;
import com.example.newsapps.presentation.adapter.MainAdapter;
import com.example.newsapps.presentation.base.BaseFragment;
import com.example.newsapps.presentation.base.BasePresenter;
import com.example.newsapps.presentation.router.Router;

import java.util.Map;

public class MainFragment extends BaseFragment<FragmentMainBinding> implements MainFragmentContract.View {
    private MainFragmentContract.Presenter presenter;
    private MainAdapter adapter;


    public MainFragment() {
    }

    public static MainFragment newInstance() {
        return new MainFragment();
    }



    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    protected void initView() {
        presenter = new MainFragmentPresenter();
        getBinding().setEvent(presenter);
        Router.getInstance().showBackStack(false);
        Router.getInstance().setAppBarText(getString(R.string.main_fragment_name_app_bar));

    }

    @Override
    protected void attachFragment() {

    }

    @Override
    protected void startFragment() {
        presenter.onStartView(this);
        presenter.init();
    }

    @Override
    protected void destroy() {
        presenter = null;

    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_main;
    }

    @Override
    public void initAdapter(String string) {
        adapter = new MainAdapter();
        getBinding().countryRV.setLayoutManager(new LinearLayoutManager(this.getContext(), LinearLayoutManager.HORIZONTAL, false));
        getBinding().countryRV.setAdapter(adapter);
    }

    @Override
    public void messageError(String msg) {
        Toast.makeText(this.getContext(),msg,Toast.LENGTH_LONG).show();
    }
}