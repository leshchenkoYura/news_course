package com.example.newsapps.presentation.fragment.main;

import com.example.newsapps.data.model.api.country.ResponseCountry;
import com.example.newsapps.domain.domain_country.InteractorCountry;
import com.example.newsapps.domain.domain_country.UseCaseCountry;
import com.example.newsapps.presentation.adapter.MainAdapter;

import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableSingleObserver;
import timber.log.Timber;

public class MainFragmentPresenter implements MainFragmentContract.Presenter, InteractorCountry.Listener {
    private MainFragmentContract.View view;
    private InteractorCountry.UseCaseCountry country;


    public MainFragmentPresenter() {
        country = new UseCaseCountry();
        country.onStart(this);
    }

    @Override
    public void onStartView(MainFragmentContract.View view) {
        this.view = view;
    }

    @Override
    public void onStopView() {
        view = null;
    }

    @Override
    public void onSuccess() {

    }

    @Override
    public void onError(String msg) {

    }

    @Override
    public void init() {
        country.getResponceCountry();
//
//        country.read()
//                .subscribe(new DisposableSingleObserver<ResponseCountry>() {
//                    @Override
//                    public void onSuccess(@NonNull ResponseCountry responseCountry) {
//                        view.initAdapter("////");
//                        dispose();
//                    }
//
//                    @Override
//                    public void onError(@NonNull Throwable e) {
//                        view.messageError(e.getMessage());
//                    }
//                });

    }
}
