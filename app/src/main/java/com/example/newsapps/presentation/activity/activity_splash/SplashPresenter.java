package com.example.newsapps.presentation.activity.activity_splash;


import com.example.newsapps.presentation.router.Router;

import java.util.concurrent.TimeUnit;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class SplashPresenter implements SplashView.Presenter {
        SplashView.View view;
        @Override
        public void onStartView(SplashView.View view) {
            this.view = view;
        }

        @Override
        public void onStopView() {
            view = null;
        }

        @Override
        public void transaction(String cmd) {
            Single.defer(() -> Single.timer(3000, TimeUnit.MILLISECONDS))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableSingleObserver<Long>() {
                        @Override
                        public void onSuccess(@NonNull Long aLong) {
                            Router.getInstance().step(cmd, null);
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            Timber.e(e.getMessage());
                        }
                    });
        }

        @Override
        public void onBackPressed() {
        Router.getInstance().onBackPressed();
    }
}
