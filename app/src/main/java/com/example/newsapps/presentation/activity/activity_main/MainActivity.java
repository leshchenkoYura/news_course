package com.example.newsapps.presentation.activity.activity_main;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.example.newsapps.R;
import com.example.newsapps.databinding.ActivityMainBinding;
import com.example.newsapps.presentation.base.BaseActivity;
import com.example.newsapps.presentation.base.BasePresenter;
import com.example.newsapps.presentation.router.Router;

public class MainActivity extends BaseActivity<ActivityMainBinding> implements MainContract.View {
    private MainContract.Presenter presenter;

    @Override
    protected void initView(@Nullable Bundle savedInstanceState) {
        presenter = new MainPresenter();
        getBinding().setEvent(presenter);
        Router.getInstance().startView(this);

    }

    @Override
    protected int getResLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void startView() {
        presenter.onStartView(this);
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void goToFragment(Fragment fragment, int container) {
        super.goToFragmentWithBackStack(fragment, container);
    }

    @Override
    public void openDialog(DialogFragment fragment, int container) {
    }

    @Override
    public void closeFragmentDialog(DialogFragment fragment) {

    }

    @Override
    public <T> void transactionActivity(Class<?> activity, boolean cycleFinish, T object) {
        super.transactionActivity(activity, cycleFinish, object);
    }

    @Override
    public void showBackStack(boolean flag) {
        getBinding().btnBackspace.setVisibility(flag ? View.VISIBLE:View.GONE);
    }

    @Override
    public void setAppBarText(String string) {
        getBinding().header.setText(string);
    }
}
