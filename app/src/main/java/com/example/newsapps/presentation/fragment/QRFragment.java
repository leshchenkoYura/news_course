package com.example.newsapps.presentation.fragment;


import android.graphics.Bitmap;
import android.view.View;

import com.example.newsapps.R;
import com.example.newsapps.databinding.FragmentQRBinding;
import com.example.newsapps.presentation.base.BaseFragment;
import com.example.newsapps.presentation.base.BasePresenter;


public class QRFragment extends BaseFragment<FragmentQRBinding> implements QrContract.View{
private QrContract.Presenter presenter;

    public QRFragment() {

    }


    public static QRFragment newInstance() {
        return new QRFragment();
    }


    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    protected void initView() {
        presenter = new QrPresenter();
        getBinding().setEvent(presenter);
        getBinding().qrView.setVisibility(View.GONE);
    }

    @Override
    protected void attachFragment() {

    }

    @Override
    protected void startFragment() {
        presenter.onStartView(this);
    }

    @Override
    protected void destroy() {
        presenter = null;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_q_r;
    }

    @Override
    public void showQr(Bitmap bitmap) {
        getBinding().qrView.setImageBitmap(bitmap);
        getBinding().qrView.setVisibility(View.VISIBLE);
    }

    @Override
    public String getInfo() {
        return String.valueOf(getBinding().appCompatEditText.getText());
    }

    @Override
    public void toast(String msg) {
        super.toastLong(msg);
    }
}