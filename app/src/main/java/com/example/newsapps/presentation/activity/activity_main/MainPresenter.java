package com.example.newsapps.presentation.activity.activity_main;

import com.example.newsapps.presentation.router.Router;

public class MainPresenter implements MainContract.Presenter {
    private MainContract.View view;
    @Override
    public void onStartView(MainContract.View view) {
        this.view = view;
        Router.getInstance().step(Router.Constant.FRAGMENT_MAIN, null);
    }

    @Override
    public void onStopView() {
        view = null;
    }

    @Override
    public void onBackPressed() {
        Router.getInstance().onBackPressed();
    }
}
