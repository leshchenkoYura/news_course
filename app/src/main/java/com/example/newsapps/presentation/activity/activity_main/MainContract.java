package com.example.newsapps.presentation.activity.activity_main;

import com.example.newsapps.presentation.base.BaseContract;
import com.example.newsapps.presentation.base.BasePresenter;

public interface MainContract {
    interface View extends BaseContract {

    }

    interface Presenter extends BasePresenter<View> {
        void onBackPressed();
    }
}
