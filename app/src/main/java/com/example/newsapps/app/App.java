package com.example.newsapps.app;

import android.app.Application;

import com.example.newsapps.BuildConfig;
import com.example.newsapps.data.local_storege.LocalStoregeImpl;
import com.example.newsapps.data.remout_storege.RestAdapter;

import timber.log.Timber;


public class App extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        LocalStoregeImpl.getInstance(this);
        RestAdapter.getInstanse();
    }

}
